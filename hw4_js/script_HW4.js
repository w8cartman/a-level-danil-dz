//      HW 4 JS
//      Task 1

class Student {
  constructor(enrollee) {
    this.id = Student.id++;
    Object.assign(this, enrollee)
    Student.listOfStudents.push(this)
    Student.sortByRating(Student.listOfStudents)
    Student.scholarship(Student.listOfStudents)
  }
  static id = 1;
  static listOfStudents = [];

  static sortByRating(arr) {
    arr.sort(function (a, b) {
      return b.ratingPoint === a.ratingPoint
        ? b.schoolPoint - a.schoolPoint
        : b.ratingPoint - a.ratingPoint;
    });
  }

  static scholarship(arr) {
    let leng = Student.listOfStudents.length - 1;
    for (let key = 0; key <= leng; key++) {
      if (arr[key].ratingPoint >= 800 && key <= 4) {
        arr[key].isSelfPayment = false;
      } else arr[key].isSelfPayment = true;
    }
  }
}

console.log(Student.listOfStudents);
const oneStudent = new Student(studentArr[0]);
const twoStudent = new Student(studentArr[1]);
const threeStudent = new Student(studentArr[2]);
const fourStudent = new Student(studentArr[3]);
const fiveStudent = new Student(studentArr[4]);
const sixStudent = new Student(studentArr[5]);
const sevenStudent = new Student(studentArr[6]);
const eightStudent = new Student(studentArr[7]);
const nineStudent = new Student(studentArr[8]);
console.log(Student.listOfStudents);


//      Task 2
// ==============================================================
class CustomString {
  constructor() {}

  reverse(str) {
    return str.split("").reverse().join("");
  }

  ucFirst(str) {
    return str[0].toUpperCase() + str.slice(1);
  }

  ucWords(str) {
    return str
      .split(/\s+/)
      .map((word) => word[0].toUpperCase() + word.substring(1))
      .join(" ");
  }
}

const myString = new CustomString();

console.log(myString.reverse("qwertyui"));
console.log(myString.ucFirst("qwertyu"));
console.log(myString.ucWords("hello my dear teacher"));

//      Task 3
// ==============================================================
class Validator {
  constructor() {}

  checkIsEmail(str) {
    return str.includes("@gmail.com");
  }

  checkIsDomain(str) {
    return str.includes(".com");
  }

  checkIsDate(str) {
    let data = str.split("-");
    if (isNaN(Date.parse(data[2] + "-" + data[1] + "-" + data[0]))) {
      return false;
    }
    return true;
  }

  checkIsPhone(str) {
    let strReplace = str.replace(/\D/g,'');
    if (strReplace.slice(0,3) === '380') {
      return `Your number ${strReplace}`;
    }
    return 'Nomer ne Ukraine';
  }
}

const validator = new Validator();

console.log(validator.checkIsEmail("vasya.pupkin@gmail.com"));
console.log(validator.checkIsDomain("google.com"));
console.log(validator.checkIsDate("12.24.2019"));
console.log(validator.checkIsPhone("+38 (066) 937-99-92"));
