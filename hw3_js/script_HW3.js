//      HW 3 JS
//      Task 1

function Emploee(emplyee) {
  Object.assign(this, emplyee);

  Object.defineProperty(this, "fullInfo", {

    //   отдать все свойства
    get: function fullInfo() {
      let result = "";

      for (let key in this) {
        result += key + " - " + this[key] + " ";
      }
      return result;
    },

    //   записать/изменить новое значение
    set: function fullInfo(data) {
      for (let key in data) {
        if (key in this) {
          this[key] = data[key];
        }
      }
    },
  });
}
// ==================================================================
//      Task 7*

const emplyeeObjGet = new Emploee(emplyeeArr[0]);
console.log(emplyeeObjGet.fullInfo);

const employeeObjSet = new Emploee(emplyeeArr[0]);
employeeObjSet.fullInfo = { name: "OLEG", salary: 9000, email: "ex@mail.ua" };
console.log(employeeObjSet);
// ==================================================================

const emplyeeObj = new Emploee({
  id: 0,
  name: "Valeriy",
  surname: "Zhmishenko",
  salary: 1000,
  workExperience: 10,
  isPrivileges: true,
  gender: "male",
});

console.log(emplyeeObj);

// ==================================================================
//      Task 2

Emploee.prototype.getFullName = function () {
  return this.surname + " " + this.name;
};
console.log(emplyeeObj.getFullName());

// =================================================================
//      Task 3

const createEmployesFromArr = (arr) => {
  return arr.map((val) => new Emploee(val));
};
const emplyeeConstructArr = createEmployesFromArr(emplyeeArr);
console.log(emplyeeConstructArr);

// ==================================================================
//      Task 4

const getFullNamesFromArr = (arr) => {
  return arr.map((val) => val.getFullName());
};

const names = getFullNamesFromArr(emplyeeConstructArr);
console.log(names);

// ==================================================================
//      Task 5

const getMiddleSalary = (arr) => {
  return Math.floor(
    arr.reduce((sum, value) => sum + value.salary, 0) / arr.length
  );
};

const midSalary = getMiddleSalary(emplyeeConstructArr);
console.log("average salary =" + " " + midSalary);

// ==================================================================
//      Task 6*

const getRandomEmployee = (arr) => {
  return arr[Math.floor(Math.random() * arr.length)];
};
console.log(getRandomEmployee(emplyeeConstructArr));