//      HW 5 JS
//      Task 1

// const getCounter = (a) => {
//     let counter = a

//     countSum =  () => {
//         let sum = 0
//         sum += counter
//         return sum
//     }
//     console.log(countSum);
// }

// const func =
// console.log(getCounter(4));

// const counter =((count=0) => num => count += num)()
// console.log(counter(2));
// console.log(counter(3));
// console.log(counter(6));

// ================================================
//          task 2

const counter = (() => {
  let arr = [];
  return (arg) => {
    if (arg !== undefined) {
      arr[arr.length] = arg;
      return arr;
    }
    return arr;
  };
})();

console.log(counter(1));
console.log(counter(34));
console.log(counter({ name: "Vasya" }));
console.log(counter());

// ================================================
//           task 3

// const timer = (time) => {
//   const timerID = setInterval(() => {
//     time--;
//     if (time === 0) {
//       clearInterval(timerID);
//       console.log("Timer end");
//     } else {
//       console.log(time);
//     }
//   }, 1000);
// };
// timer(5);

// ================================================
//           task 4

const parseTime = time => {
  let sec = time % 60;
  let min = (time - sec) / 60
  return `${min > 9 ? '': '0'}${min} : ${sec > 9 ? '': '0'}${sec}`
};

const timer = (time) => {
  const timerID = setInterval(() => {
    time--;
    if (time === 0) {
      clearInterval(timerID);
      console.log("Time End");
      return;
    }
    console.log(parseTime(time));
  }, 1000);
};

timer(10)