//      lesson 24.05.21

//      вызвали екземпляр
function Emploee(obj) {
  Object.assign(this, obj);

  Object.defineProperty(this, "fullInfo", {
    //   отдать все свойства
    get: function () {
      let result = "";

      for (let key in this) {
        result += key + " - " + this[key] + " ";
      }
      return result;
    },

    //   записать/изменить новое значение
    set: function (data) {
      for (let key in data) {
        if (key in this) {
          this[key] = data[key];
        }
      }
    },
  });
}

// все через точку (.prototype) - это исчесляемая переменная
// Emploee.prototype.getFullName = function(){}

Object.defineProperty(Emploee.prototype, "getFullName", {
  value: function () {},
  enumerable: false,
});

const employee = new Emploee(emplyeeArr[0]);
console.log("employee:", employee);
// ==================================================================

const exchangeData = {
  usd: 28,
  eur: 32,
  rub: 0.35,
};

class Wallet {
  constructor(initialAmmount) {
    this.ammount = initialAmmount;
  }

  _getCurrencyInUah(currency, amount) {
    return exchangeData[currency] * amount;
  }

  // getExchangedMoney(currency, amount)
  // 1 первый аргумент передаёт тип валюты(строка)
  // 2 аргумент передаёт кол - во этой валюты
  // 3 преести валютув гривны Вычесть полученное кол - во грн из кошелька
  // После вернкть полученоекол - во гривен
  getExchMoney(currency, amount) {
    const moneyInUA = this._getCurrencyInUah(currency, amount);
    return (this.ammount -= moneyInUA);
  }

  // setExchangedMoney(currency, amount)
  // 1 первый аргумент передаёт тип валюты(строка)
  // 2 аргумент передаёт кол - воэтой валюты
  // 3 преести валютув гривныю Добавить полученное кол - во грн из кошелька
  // После вернкть полученоекол - во гривен
  setExchMoney(currency, amount) {
    const moneyInUA = this._getCurrencyInUah(currency, amount);
    return (this.ammount += moneyInUA);
  }
}

class eWallet extends Wallet {
  constructor(name, initialAmmount) {
    super(initialAmmount);
    this.numberCard = "4569 5674 5349 9089";
    this.name = name;
  }

  get typeOfCard(){
      return this.numberCard.startsWith('4') ? 'MasterCared' : 'Visa'
  }

  getCardInfo(keysArr){
      const data = {}
    for(let key of keysArr){
        data[key] = this[key]
    }
    return data
  }
}

// wallet.exchangeData(['typeOfCard', 'numberCard'])

// const wallet = new Wallet(1000);

const wallet = new eWallet("Monobank", 1000);
