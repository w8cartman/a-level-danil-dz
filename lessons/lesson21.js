//      lesson 07.06.21
//      Methods

function Random() {
  this.value = Math.random();
  return this.value;
}

// const parse = (str) => {
//   return str[0].toUpperCase() + str.slice(1).toLowerCase();
// };

// const Trim = (str) => {
//   return str.trim();
// };

// let obj = { a: 1 };

// let obj1 = { b: 2 };

// Object.customAssign = function (obj, ...args) {
//   for (let arg of arg) {
//     for (let key in arg) {
//       obj[key] = arg[key];
//     }
//   }
//   return obj;
// };

const arr = [1, 2, 3, 4, 5, 6, 7, 8];

Object.defineProperty(Array.prototype, 'c_forEach', {
    value: function(callback){
        for(let key in this){
            callback(this[key], +key, this)
        }
    },
    enumerable: false
})
arr.c_forEach(item => console.log(item))

