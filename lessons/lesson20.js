//      lesson 03.06.21
//      Замыкания

// const initCount = (count) => {
//   const obj = {
//       counter1: () => ++count,
//       counter2: () => ++count,
//   }
//   return obj
// }
// const {counter1, counter2} = initCount(0)

// ===============================================================
// const initCount = (count) => ({
//     counter1: () => ++count,
//     counter2: () => ++count,
// })
// const { counter1, counter2 } = initCount(0);

// ===============================================================
// const counter = ((count=0) => () => ++count)(10)

// ===============================================================
const counter = () => {
    let dateOfLastCall = new Date().getTime()

    return () => {
        const result = new Date().getTime() - dateOfLastCall
        dateOfLastCall = new Date().getTime()
        return ~~(result / 1000)
        // ~~ работает как Math.floor - округляет число до точки
    }
}
const timeDiff = counter()

// ===============================================================
// const timer = (time) => {
//   const timerID = setInterval(() => {
//     time--;
//     if (time === 0) {
//       clearInterval(timerID);
//       console.log("Timer end");
//     } else {
//       console.log(time);
//     }
//   }, 1000);
// };
// timer(5);

// ===============================================================
