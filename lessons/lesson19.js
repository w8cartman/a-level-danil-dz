//      lesson 27.05.21

class Hero {
  constructor(name, xp) {
    this._name = name;
    this._id = Hero.id++;
    this._xp = xp;
  }
  static id = 1;

  static getName() {
    return 5
  }

  get id() {
    return this._id;
  }

  get xpUp() {
    return this._xp;
  }

  set xpUp(value) {
    this._xp = value * this._xp;
  }
}

class Warior extends Hero {
  constructor(name, xp, weapon) {
    super(name, xp);

    this.weapon = weapon;
  }
}

const warior = new Warior("Andrey", 25, "axe");
const mag = new Warior("Sergey", 30, "palka")
const orc = new Warior("Jenya", 10, "KAMENb")

mag.xpUp = 2;
console.log(warior);
console.log(mag);
console.log(orc);
console.log(warior.getName);
console.log(mag.id);