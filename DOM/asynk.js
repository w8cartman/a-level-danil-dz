// let result = new Promise((resolve, reject) => {
//     console.log('Promise executing');
//     resolve('webdraftt');
//   });
//   result.then(result => console.log('Result: ', result));
//   setTimeout(() => result.then(result => console.log('Again: ', result)), 1000);

//    AXIOS

class Api {
  constructor() {
    this.axios = axios.create({
      baseURL: "https://hipstagram-api.herokuapp.com",
      headers: {
        "Content-Type": "application/json",
      },
    });
  }

  async login(body) {
    try {
      const { data } = await this.axios.post("/auth/login", body);
      localStorage.setItem("token", data.access_token);
    } catch (e) {
      console.log(e);
    }
  }

  async getCurentUser() {
    try {
      const { data } = await this.axios.get("/users/current", {
        headers: {
          "Authorization": localStorage.getItem("token"),
        },
      });
      console.log(data);
    } catch (e) {
      console.log(e);
    }
  }
}

const api = new Api();

const runAsynk = async () => {
  await api.login({
    "login": "AndreyRem",
    "password": "qwerty123",
  })
  await api.getCurentUser()
};

runAsynk()