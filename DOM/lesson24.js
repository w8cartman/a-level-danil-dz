//          lesson 14.06.21
//          lesson 17.06.21

// const collection = document.querySelectorAll("div");
// collection.forEach((item) => (item.innerText = "qwertyu"));

const titleElement = document.querySelector(".title");

titleElement.className += " active";
titleElement.className += " newClass";
titleElement.className += " qwertyu";

titleElement.classList.remove();

// =========================================================

const user1 = {
  name: "Vasya",
  getName: function () {
    return this.name;
  },
};

const user2 = {
  name: "Petya",
};

console.log(user1);

const later = ["a", "b", "c", "d", "e", "f"];

const hexGen = () => {
  
  let HEX = "#";
  for (i = 0; i < 6; i++) {
    const whatFirst = Math.floor(Math.random() * (3 - 1)) + 1;
    let rand1 = Math.floor(Math.random() * (10 - 1)) + 1;
    let randLater = Math.floor(Math.random() * (6 - 1)) + 1;
    if (whatFirst === 1) {
      HEX += rand1;
    } else {
      HEX += later[randLater];
    }
  }
  return HEX
};

for( let i=0; i<6; i++){
    const hex = hexGen()
    const div = document.createElement('div')
    div.className = 'card'
    div.style.backgroundColor = hex
    div.innerText = hex
    document.body.appendChild(div)
}


// ========================================
// create Element
const cEl = (tag) =>{
    return document.createElement(tag)
}
// get Element
const gEl = (selector) =>{
    return document.querySelector(selector)
}

const btn = cEl('button')

class Render {
  constructor(){
    this.container = document.querySelector('.container')
  }

  _renderACrd(data){
    const card = document.createElement('div')
    card.className = 'card'

    const favorites = document.createElement('button')
    favorites.className = 'favorites'
    favorites.innerText = 'Favorite'
    favorites.onclick = () => console.log(data, 'Favvorite')

  } 
}
