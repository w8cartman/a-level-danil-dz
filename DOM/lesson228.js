class BindEvent {
  constructor() {
    this.searchInput = document.getElementById("searchInput");
    this.BindEventOnInput();
  }

  BindEventOnInput() {
    this.searchInput.oninput = (event) => {
      const { value } = event.target;
      const filteredItems = items.filter((item) => {
        return item.name;
      });
    };
  }
}

// const outpuEvents extends BindEvent{}

const inpt = new BindEvent();

class Card {
  constructor() {
    this.container = document.querySelector(".cards");
    this.renderCards();
  }

  _createCards(data) {
    const card = document.createElement("div");
    card.className = "card";

    card.innerHTML = ` 
            <img class="card-img" src="images/${data.imgUrl}" alt=""> 
            <h3 class="aviable">${data.name}</h3> 
            <p class="infText">${data.orderInfo.inStock} left in stock</p> 
            <p class="infText">Prise: ${data.price} $</p> 
      `;

    const addToCart = document.createElement("button");
    addToCart.className = "card-btn";
    addToCart.innerHTML = "Add to Cart";
    addToCart.onclick = () => console.log(data, "addToCart");
    card.appendChild(addToCart);

    const footCrard = document.createElement("div");
    footCrard.className = "card-footer";
    card.appendChild(footCrard);

    footCrard.innerHTML = ` 
            <p class="card-footer-text">${
              data.orderInfo.reviews
            }% Positive reviews</p> 
            <p class="card-footer-text">${Math.floor(
              Math.random() * 900 + 100
            )} orders</p> 
      `;

    return card;
  }

  renderCards(arr = items) {
    this.container.innerHTML = "";
    const cards = arr.map((card) => this._createCards(card));
    this.container.append(...cards);
  }
}

const cards = new Card();

class FilterService {
    constructor() {
        this.price = []
        this.os = []
        this.color = []
    }

    changePrice(value, type) {
        if (type === 'from'){
            this.price[0] = value;
        }
        else {
            this.price[1] = value
        }
    }

    changeColor(color) {
        if(this.color.includes(color)) {
            this.color = this.color.filter(c => c !==color)
        } else {
            this.color.push(color)
        }
    }

    changeOs(os) {
        if(this.os.includes(os)) {
            this.os = this.os.filter(c => c !==os)
        } else {
            this.os.push(color)
        }
    }

    filterArrByOptions() {
        return items.filter(item => {
            if (!(item.price >= this.price[0] && item.price <= this.price[1])) return
            
            // let isColor = false
            // for (let color of this.color) {
            //     if(item.color.includes(color)){
            //         isColor = true
            //         break
            //     }
            // }
            // if(!isColor) return
            // 2 variant

            let isColor = this.color.some(c => item.color.includes(color))
            if(!isColor) return
            
            let isOs = this.os.some.includes(item.os)
            if(!isColor) return

            return true

        })
    }

    filterAndRender() {
        const filterx
    }

}

// filter Render
class filterRender {
  constructor() {
    this.filterService = new FilterService()
    this.options = [
      {
        title: "Price",
        options: [],
        setter: this.filterService.changePrice.bind(this.filterService),
      },

      {
        title: "Color",
        options: [],
        setter: this.filterService.changeColor.bind(this.filterService),
      },

      {
        title: "OS",
        options: [],
        setter: this.filterService.changeOs.bind(this.filterService),
      },

      this.renderFilters
    ];
  }

  get initialPrice() {
      const sortedArr = [...items].sort((a,b)) = a.price - b.price
      return [
          sortedArr[0].price,
          sortedArr[sortedArr.lenght -1].price
      ]
  }

  get initialColor(){
      return items.reduce((acc, item) => {
          items.color.forEach(colorInArr =>{
            if (acc.includes(colorInArr)) return
            acc.push(colorInArr)
          })
          return acc
      }, [])
  }

  get initialColor(){
      return items.reduce((acc, item) => {
            if (item.os !== null && !acc.includes(item.os)) {
                acc.push(item.os)
            }
          return acc
      }, [])
  }

  renderFilters(){
    const filterContainer = document.querySelector('.filter')
    this.options.forEach(data =>{
        const block = document.createElement('div')
        block.className ='filter_block'

        const header = document.createElement('div')
        block.className ='filter_header';
        header.innerHTML = `<strong> ${data.title}</strong>`
        heaser.onclick = () => {
            container.classList.toggle('filter__container--active')
            header.innerHTML =
            `
            <strong> ${data.title}</strong>

            `
        }

        const container = document.createElement('div')
        block.className ='filter_container'

        block.append(header, container)
        filtersContainer.appendChild(block)
    })
  }
}

const renderFilters = new filterRender()